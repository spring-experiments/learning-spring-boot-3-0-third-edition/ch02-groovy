package org.elu.learn.spring.boot3.ch02

import org.springframework.stereotype.Service

@Service
class VideoService {
    private def videos = [
        new Video('Need HELP with your SPRING BOOT 3 App?'),
        new Video('Don\'t do THIS to your own CODE!'),
        new Video('SECRETS to fix BROKEN CODE!')
    ]

    List<Video> getVideos() {
        videos
    }

    Video create(Video newVideo) {
        this.videos << newVideo
        newVideo
    }
}
