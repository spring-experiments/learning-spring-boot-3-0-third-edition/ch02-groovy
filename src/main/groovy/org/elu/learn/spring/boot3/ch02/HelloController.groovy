package org.elu.learn.spring.boot3.ch02

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.PostMapping

@Controller
class HelloController {
    private final VideoService videoService

    HelloController(VideoService videoService) {
        this.videoService = videoService
    }

    @GetMapping('/')
    String index(Model model) {
        model.addAttribute('videos', videoService.videos)
        'index'
    }

    @PostMapping('/new-video')
    String newVideo(@ModelAttribute Video newVideo) {
        videoService.create(newVideo)
        'redirect:/'
    }
}
