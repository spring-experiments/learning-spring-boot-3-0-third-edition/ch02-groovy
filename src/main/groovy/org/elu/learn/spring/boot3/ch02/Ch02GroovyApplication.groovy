package org.elu.learn.spring.boot3.ch02

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class Ch02GroovyApplication {

    static void main(String[] args) {
        SpringApplication.run(Ch02GroovyApplication, args)
    }

}
