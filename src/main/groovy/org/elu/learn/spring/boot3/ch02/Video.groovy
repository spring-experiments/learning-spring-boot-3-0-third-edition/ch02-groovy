package org.elu.learn.spring.boot3.ch02

record Video(String name) {}

// NOTE: record wasn't working with @AttributeModel but work fine with JSON-base API.
// So for template engine have to use the class defined below
//class Video {
//    String name
//
//    Video() {}
//
//    Video(String name) {
//        this.name = name
//    }
//}
