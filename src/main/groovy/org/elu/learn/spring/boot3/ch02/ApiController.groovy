package org.elu.learn.spring.boot3.ch02

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping('/api/videos')
class ApiController {
    private final VideoService videoService

    ApiController(VideoService videoService) {
        this.videoService = videoService
    }

    @GetMapping
    List<Video> all() {
        videoService.videos
    }

    @PostMapping
    Video newVideo(@RequestBody Video newVideo) {
        videoService.create(newVideo)
    }
}
